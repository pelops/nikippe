import nikippe.schema.achart


def get_schema():
    return nikippe.schema.achart.get_schema("sequentialchart", "Regular chart with the latest values added at the "
                                                               "right and the oldest values on the left.")

