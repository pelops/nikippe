# Image Chops Module Examples

generate for each PIL.[ImageChops](https://pillow.readthedocs.io/en/3.1.x/reference/ImageChops.html) method an example 
result.

The following two images are used for the examples as inputs:
![base_a](ImageChops_base_a.png) 
![base_b](ImageChops_base_b.png)

## PIL.ImageChops.add(image1, image2, scale=1.0, offset=0)

![add](ImageChops_add.png)

## PIL.ImageChops.add_modulo(image1, image2)

![add_module](ImageChops_add_modulo.png)

## PIL.ImageChops.blend(image1, image2, alpha)

![blend](ImageChops_blend.png)
alpha = 2

## PIL.ImageChops.composite(image1, image2, mask)

![blend](ImageChops_composite.png)

mask = all pixels in image1 with the same color as image 2

## PIL.ImageChops.constant(image, value)

![blend](ImageChops_constant.png)

value = 200

## PIL.ImageChops.darker(image1, image2)

![blend](ImageChops_darker.png)

## PIL.ImageChops.difference(image1, image2)

![blend](ImageChops_difference.png)

## PIL.ImageChops.duplicate(image)

![blend](ImageChops_duplicate.png)

## PIL.ImageChops.invert(image)

![blend](ImageChops_invert.png)

## PIL.ImageChops.lighter(image1, image2)

![blend](ImageChops_lighter.png)

## PIL.ImageChops.logical_and(image1, image2)

![blend](ImageChops_logical_and.png)
Note: images must be of mode="1"

## PIL.ImageChops.logical_or(image1, image2)

![blend](ImageChops_logical_or.png)
Note: images must be of mode="1"

## PIL.ImageChops.multiply(image1, image2)

![blend](ImageChops_multiply.png)

## PIL.ImageChops.offset(image, xoffset, yoffset=None)

![blend](ImageChops_offset.png)

xoffset = 20, yoffset = 20

## PIL.ImageChops.screen(image1, image2)

![blend](ImageChops_screen.png)

## PIL.ImageChops.subtract(image1, image2, scale=1.0, offset=0)

![blend](ImageChops_subtract.png)

## PIL.ImageChops.subtract_modulo(image1, image2)

![blend](ImageChops_subtract_modulo.png)




