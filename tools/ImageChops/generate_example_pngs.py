from PIL import ImageChops, Image, ImageDraw, ImageMath

# generate for each ImageChops method an example result.
# https://pillow.readthedocs.io/en/3.1.x/reference/ImageChops.html

bar_width = 5
width = 256*bar_width
height = 60
transparent_color = 125

a = Image.new('L', (width, height), transparent_color)
b = Image.new('L', (width, height), transparent_color)
white = Image.new('L', (width, height), 255)

draw = ImageDraw.Draw(a)
for i in range(256):
    x1 = i * bar_width
    x2 = x1 + bar_width - 1
    y1 = 0
    y2 = height - 1
    draw.rectangle((x1, y1, x2, y2), fill=i)

a.save("ImageChops_base_a.png")
b.save("ImageChops_base_b.png")

try:
    result = ImageChops.add(a, b).save("ImageChops_add.png")
except Exception as e:
    print("add", e)
try:
    result = ImageChops.add_modulo(a, b).save("ImageChops_add_modulo.png")
except Exception as e:
    print("add_modulo", e)
try:
    result = ImageChops.blend(a, b, 2).save("ImageChops_blend.png")
except Exception as e:
    print("blend", e)
try:
    mask = ImageChops.difference(a, b)
    mask = ImageMath.eval("convert(min(a*255, 255), 'L')", a=mask)
    result = ImageChops.composite(a, b, mask).save("ImageChops_composite.png")
except Exception as e:
    print("composite", e)
try:
    result = ImageChops.constant(a, 200).save("ImageChops_constant.png")
except Exception as e:
    print("constant", e)
try:
    result = ImageChops.darker(a, b).save("ImageChops_darker.png")
except Exception as e:
    print("darker", e)
try:
    result = ImageChops.difference(a, b).save("ImageChops_difference.png")
except Exception as e:
    print("difference", e)
try:
    result = ImageChops.duplicate(a).save("ImageChops_duplicate.png")
except Exception as e:
    print("duplicate", e)
try:
    result = ImageChops.invert(a).save("ImageChops_invert.png")
except Exception as e:
    print("invert", e)
try:
    result = ImageChops.lighter(a, b).save("ImageChops_lighter.png")
except Exception as e:
    print("lighter", e)
try:
    result = ImageChops.logical_and(a.convert(mode="1"), b.convert(mode="1")).save("ImageChops_logical_and.png")
except Exception as e:
    print("logical_and", e)
try:
    result = ImageChops.logical_or(a.convert(mode="1"), b.convert(mode="1")).save("ImageChops_logical_or.png")
except Exception as e:
    print("logical_or", e)
try:
    result = ImageChops.multiply(a, b).save("ImageChops_multiply.png")
except Exception as e:
    print("multiply", e)
try:
    result = ImageChops.offset(a, 40, 80).save("ImageChops_offset.png")
except Exception as e:
    print("offset", e)
try:
    result = ImageChops.screen(a, b).save("ImageChops_screen.png")
except Exception as e:
    print("screen", e)
try:
    result = ImageChops.subtract(a, b).save("ImageChops_subtract.png")
except Exception as e:
    print("subtract", e)
try:
    result = ImageChops.subtract_modulo(a, b).save("ImageChops_subtract_modulo.png")
except Exception as e:
    print("subtract_modulo", e)

