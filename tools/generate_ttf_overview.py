from PIL import ImageFont
from PIL import ImageDraw
from PIL import Image

blind_image = Image.new("1", (1000, 1000), 255)
blind_draw = ImageDraw.Draw(blind_image)

font_overview = {}

with open("ttf_list.txt") as f:
    font_list = [x.strip('\n') for x in f.readlines()]

font_sizes = [8, 14, 77]
texts = ["11:11", "00:00"]
for font_filename in font_list:
    print("processing '{}'.".format(font_filename))
    font_name = (font_filename.split("/")[-1]).split(".")[0]
    font_overview[font_name] = {}

    for font_size in font_sizes:
        font_overview[font_name][font_size] = {}
        for text in texts:
            img_filename = str(font_size) + "pt-" + text.replace(":", "_") + "-" + font_name + ".png"
            img_font = ImageFont.truetype(font_filename, font_size)
            w, h = blind_draw.textsize(text, img_font)
            image = Image.new("1", (w, h), 255)
            draw = ImageDraw.Draw(image)
            draw.text((0, 0), text, font=img_font, fill=0)
            image.save(img_filename)
            font_overview[font_name][font_size][text] = {
                "width": w,
                "height": h,
                "ratio": w/h,
                "text": text,
                "size": font_size,
                "filename": img_filename
            }
            print("Saved {}.".format(img_filename))

with open("ttf_overview.html", "w") as f:
    f.write("<html>")
    f.write("<head>")
    f.write('<link rel="stylesheet" href="stylesheet.css">')
    f.write("</head>")
    f.write("<body>")
    f.write("<table>")
    f.write("<tr>")
    f.write("<th>Font</th>")
    for font_size in font_sizes:
        for text in texts:
            f.write("<th colspan=2>'{}'@{}pt</th>".format(text, font_size))
    f.write("</tr>")
    fn = sorted(font_overview.keys())
    for font_name in fn:
        f.write("<tr>")
        f.write("<td>{}</td>".format(font_name))
        for font_size in font_sizes:
            for text in texts:
                value = font_overview[font_name][font_size][text]
                f.write("<td>{:.2f}</td><td><img src='{}'/></td>".format(value["ratio"], value["filename"]))
        f.write("</tr>")
    f.write("</table>")

    f.write("</body></html>")


