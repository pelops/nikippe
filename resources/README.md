 * gui_background.png by tgd1975 is licensed under CC BY-NC-SA 3.0 AT
 * icon_morning.png - This file is licensed under the Creative Commons Attribution 4.0 International license. Attribution: Shannon E Thomas/toicon.com https://commons.wikimedia.org/wiki/File:Toicon-icon-lines-and-angles-rise.svg
 * icon_day - This file is licensed under the Creative Commons Attribution-Share Alike 4.0 International license. https://commons.wikimedia.org/wiki/File:Day_and_night_icon.png
 * icon_night - This file is licensed under the Creative Commons Attribution-Share Alike 4.0 International license. https://commons.wikimedia.org/wiki/File:Day_and_night_icon.png
 * icon_clock - CC0 1.0
 * icon_snow - CC0 1.0