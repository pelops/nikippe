import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from pelops import mymqttclient, myconfigtools
from pelops.logging import mylogger
from nikippe.renderer.bar import Bar, Orientation
from time import sleep
import threading


try:
    os.mkdir("bar")
except FileExistsError:
    pass

config = myconfigtools.read_config("../tests_unit/config.yaml")

logger = mylogger.create_logger(config["logger"], __name__)
mqtt_client = mymqttclient.MyMQTTClient(config["mqtt"], logger)
mqtt_client.connect()
update_available = threading.Event()
bar = Bar(config["display-server"]["renderer"]["elements"][1], update_available, mqtt_client, logger)
bar.start()
print("-----------------------------------------------------------------------")
mqtt_client.publish("/test/humidity", 20.0)
sleep(1)
bar._draw_border = True
bar._orientation = Orientation.UP
bar._update_bar_size()
bar.update_image()
bar.img.save("bar/test_bar_up_border.png")
bar._orientation = Orientation.DOWN
bar._update_bar_size()
bar.update_image()
bar.img.save("bar/test_bar_down_border.png")
bar._orientation = Orientation.LEFT
bar._update_bar_size()
bar.update_image()
bar.img.save("bar/test_bar_left_border.png")
bar._orientation = Orientation.RIGHT
bar._update_bar_size()
bar.update_image()
bar.img.save("bar/test_bar_right_border.png")
print("-----------------------------------------------------------------------")
bar._draw_border = False
bar._orientation = Orientation.UP
bar._update_bar_size()
bar.update_image()
bar.img.save("bar/test_bar_up_noborder.png")
bar._orientation = Orientation.DOWN
bar._update_bar_size()
bar.update_image()
bar.img.save("bar/test_bar_down_noborder.png")
bar._orientation = Orientation.LEFT
bar._update_bar_size()
bar.update_image()
bar.img.save("bar/test_bar_left_noborder.png")
bar._orientation = Orientation.RIGHT
bar._update_bar_size()
bar.update_image()
bar.img.save("bar/test_bar_right_noborder.png")
print("-----------------------------------------------------------------------")
bar._orientation = Orientation.UP
bar._draw_border = True
bar._update_bar_size()
for i in range(int(bar._min_value), int(bar._max_value+1)):
    bar._current_value = i
    bar.update_image()
    bar.img.save("bar/test_bar_{}.png".format(i))
