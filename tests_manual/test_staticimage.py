import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from pelops import myconfigtools
from pelops.logging import mylogger
from nikippe.renderer.staticimage import StaticImage
import threading

try:
    os.mkdir("staticimage")
except FileExistsError:
    pass

config = myconfigtools.read_config("../tests_unit/config.yaml")

logger = mylogger.create_logger(config["logger"], __name__)
update_available = threading.Event()
st = StaticImage(config["display-server"]["renderer"]["elements"][5], update_available, logger)
st.start()
st.update_image()
st.img.save("staticimage/test_staticimage.png")