import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from pelops import mymqttclient, myconfigtools
from pelops.logging import mylogger
from nikippe.renderer.mqtttext import MQTTText
from time import sleep
import threading

try:
    os.mkdir("mqtttext")
except FileExistsError:

    pass
config = myconfigtools.read_config("../tests_unit/config.yaml")

logger = mylogger.create_logger(config["logger"], __name__)

mqtt_client = mymqttclient.MyMQTTClient(config["mqtt"], logger)
mqtt_client.connect()
update_available = threading.Event()
mqttt = MQTTText(config["display-server"]["renderer"]["elements"][3], update_available, mqtt_client, logger)
mqttt.start()
mqtt_client.publish("/test/humidity", 15.0)
sleep(1)
mqttt.update_image()
mqttt.img.save("mqtttext/test_mqtttext.png")