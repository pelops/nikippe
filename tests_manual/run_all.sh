#!/bin/bash
echo ____________ test_8bit _______________________
python3 test_8bit.py
echo _
echo ____________ test_bar ________________________
python3 test_bar.py
echo _
echo ____________ test_circularchart ______________
python3 test_circularchart.py
echo _
echo ____________ test_digitalclock _______________
python3 test_digitalclock.py
echo _
echo ____________ test_displayserver ______________
python3 test_displayserver.py
echo _
echo ____________ test_elementfactory _____________
python3 test_elementfactory.py
echo _
echo ____________ test_mqtttext ___________________
python3 test_mqtttext.py
echo _
echo ____________ test_mqttimage __________________
python3 test_mqttimage.py
echo _
echo ____________ test_renderer ___________________
python3 test_renderer.py
echo _
echo ____________ test_sequentialchart ____________
python3 test_sequentialchart.py
echo _
echo ____________ test_statictext _________________
python3 test_statictext.py
echo _
echo ____________ test_staticimage ________________
python3 test_staticimage.py
echo _
echo ____________ test_imagelist __________________
python3 test_imagelist.py

