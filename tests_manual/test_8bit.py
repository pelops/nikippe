import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from pelops import mymqttclient, myconfigtools
from pelops.logging import mylogger
from nikippe.renderer.renderer import Renderer
from nikippe.renderer.sequentialchart import SequentialChart
import random
import time


# https://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console
# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', msg=None):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    if msg is None:
        print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    else:
        print('\r%s |%s| %s%% %s %s' % (prefix, bar, percent, suffix, msg), end='\r')
    # Print New Line on Complete
    if iteration == total:
        print()


def send_testdata(mqtt_client, topic, aggregation_to_history):
    random.seed()
    value = 16
    value_min = 5
    value_max = 23
    step = -0.1
    amount = 220
    max_reached = True
    min_reached = True
    for counter in range(0, amount):
        printProgressBar(counter, amount)
        mqtt_client.publish(topic, value)

        if counter % 2 == 0:
            time.sleep(0.1)
            aggregation_to_history(time.time())

        diff_from_center = abs((value_max-value_min)-(value-value_min))
        norm_diff_from_center = diff_from_center / (value_max-value_min)
        rand_max = 3 + (1-norm_diff_from_center) * 7

        value += step
        if value < value_min:
            value = value_min
            step = -step
            min_reached = True
        elif value > value_max:
            value = value_max
            step = -step
            max_reached = True
        elif min_reached and max_reached and random.random()*rand_max <= 1:
            step = -step
    printProgressBar(amount, amount)


try:
    os.mkdir("8bit")
except FileExistsError:
    pass

config = myconfigtools.read_config("../tests_unit/config.yaml")

del config["display-server"]["renderer"]["background"]
config["display-server"]["renderer"]["background-color"] = 60

for i in range(0, len(config["display-server"]["renderer"]["elements"])):
    a = 0
    b = 0
    while abs(a-b) < 30:
        a = random.randint(0, 255)
        b = random.randint(0, 255)
        if a == b:
            b = 255 - a
    config["display-server"]["renderer"]["elements"][i]["background-color"] = a
    config["display-server"]["renderer"]["elements"][i]["foreground-color"] = b
    # config["display-server"]["renderer"]["elements"][i]["active"] = True

logger = mylogger.create_logger(config["logger"], __name__)

mqtt_client = mymqttclient.MyMQTTClient(config["mqtt"], logger)
mqtt_client.connect()

renderer = Renderer(config["display-server"]["renderer"], mqtt_client, logger)
renderer._background_image
element_chart = renderer._elements[0]
if not isinstance(element_chart, SequentialChart):
    raise ValueError("Element at position 0 is not of type 'SequentialChart' (instead: '{}').".format(type(element_chart)))
renderer.start()

send_testdata(mqtt_client, "/test/humidity", element_chart._history_service._aggregation_to_history)

time.sleep(1)

print("Renderer update available: '{}'.".format(renderer.update_available.isSet))
renderer.update()
renderer.current_image.save("8bit/test_8bit.png")
renderer.stop()
mqtt_client.disconnect()
print(" --------- done ---------- ")
