import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from pelops import mymqttclient, myconfigtools
from pelops.logging import mylogger
from nikippe.renderer.mqttimage import MQTTImage
from PIL import Image
import threading
from io import BytesIO


def image_to_bytes(image):
    """Convert a PIL.Image instance to bytes - the format nedded if the mqtt payload consists of only the image."""
    bytes_image = BytesIO()
    image.save(bytes_image, format="png")
    result = bytes_image.getvalue()
    return result

try:
    os.mkdir("mqttimage")
except FileExistsError:
    pass

config = myconfigtools.read_config("../tests_unit/config.yaml")

logger = mylogger.create_logger(config["logger"], __name__)
mqtt_client = mymqttclient.MyMQTTClient(config["mqtt"], logger)
mqtt_client.connect()
update_available = threading.Event()
st = MQTTImage(config["display-server"]["renderer"]["elements"][7], update_available, mqtt_client, logger)
st.start()
st.update_image()
st.img.save("mqttimage/test_mqttimage_blank.png")

img = Image.open("../resources/icon_clock.png")
bytes = image_to_bytes(img)
mqtt_client.publish(st._topic_sub, bytes)
update_available.wait()
update_available.clear()
st.update_image()
st.img.save("mqttimage/test_mqttimage_clock.png")

img = Image.open("../resources/icon_day.png")
bytes = image_to_bytes(img)
mqtt_client.publish(st._topic_sub, bytes)
update_available.wait()
update_available.clear()
st.update_image()
st.img.save("mqttimage/test_mqttimage_day.png")

mqtt_client.publish(st._topic_sub, "")
update_available.wait()
update_available.clear()
st.update_image()
st.img.save("mqttimage/test_mqttimage_empty.png")


