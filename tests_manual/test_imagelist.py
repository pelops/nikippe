import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from pelops import mymqttclient, myconfigtools
from pelops.logging import mylogger
from nikippe.renderer.imagelist import ImageList
import threading

try:
    os.mkdir("imagelist")
except FileExistsError:
    pass

config = myconfigtools.read_config("../tests_unit/config.yaml")

logger = mylogger.create_logger(config["logger"], __name__)
mqtt_client = mymqttclient.MyMQTTClient(config["mqtt"], logger)
mqtt_client.connect()
update_available = threading.Event()
st = ImageList(config["display-server"]["renderer"]["elements"][6], update_available, mqtt_client, logger)
st.start()
st.update_image()
st.img.save("imagelist/test_imagelist_default.png")
update_available.clear()

mqtt_client.publish(st._topic_sub, "clock")
update_available.wait()
update_available.clear()
st.update_image()
st.img.save("imagelist/test_imagelist_clock.png")

mqtt_client.publish(st._topic_sub, "day")
update_available.wait()
update_available.clear()
st.update_image()
st.img.save("imagelist/test_imagelist_day.png")

mqtt_client.publish(st._topic_sub, "morning")
update_available.wait()
update_available.clear()
st.update_image()
st.img.save("imagelist/test_imagelist_morning.png")

mqtt_client.publish(st._topic_sub, "snow")
update_available.wait()
update_available.clear()
st.update_image()
st.img.save("imagelist/test_imagelist_snow.png")

mqtt_client.publish(st._topic_sub, "night")
update_available.wait()
update_available.clear()
st.update_image()
st.img.save("imagelist/test_imagelist_night_1.png")

mqtt_client.publish(st._topic_sub, "night")
update_available.wait()
update_available.clear()
st.update_image()
st.img.save("imagelist/test_imagelist_night_2.png")

#try:
#    mqtt_client.publish(st._topic_sub, "nope")
#    update_available.wait()
#    update_available.clear()
#    st.update_image()
#    st.img.save("imagelist/test_imagelist_nope.png")
#except KeyError:
#    pass
