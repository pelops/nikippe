import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from pelops import mymqttclient, myconfigtools
from pelops.logging import mylogger
from nikippe.renderer.elementfactory import ElementFactory
import threading


try:
    os.mkdir("elementfactory")
except FileExistsError:
    pass

config = myconfigtools.read_config("../tests_unit/config.yaml")

logger = mylogger.create_logger(config["logger"], __name__)

mqtt_client = mymqttclient.MyMQTTClient(config["mqtt"], logger)
mqtt_client.connect()
update_available = threading.Event()
elements = ElementFactory.create_elements(config["display-server"]["renderer"]["elements"], update_available,
                                          mqtt_client, logger)
for e in elements:
    try:
        print("===================================================================================")
        print("=                                        {}".format(e.name))
        print("===================================================================================")
        e.start()
        e.update_image()
        e.img.save("elementfactory/test_elementfactory_{}.{}.png".format(e.__class__.__name__, e.name))
    except NotImplementedError:
        pass
print("===================================================================================")
print("done. please note that no mqtt-message has been used -> AElementMQTT based renderer")
print("will probably produce blank images.")