import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from pelops import mymqttclient, myconfigtools
from pelops.logging import mylogger
from nikippe.renderer.sequentialchart import SequentialChart
import threading


try:
    os.mkdir("sequentialchart")
except FileExistsError:
    pass


config = myconfigtools.read_config("../tests_unit/config.yaml")

logger = mylogger.create_logger(config["logger"], __name__)
mqtt_client = mymqttclient.MyMQTTClient(config["mqtt"], logger)
mqtt_client.connect()
update_available = threading.Event()
chart = SequentialChart(config["display-server"]["renderer"]["elements"][0], update_available, mqtt_client, logger)
print("-----------------------------------------------------------------------")
chart.start()
chart._border_bottom = True
chart._border_top = True
chart._border_left = True
chart._border_right = True
chart._update_margins()
value = 0
step = -1
for counter in range(0, 300):
    chart._history_service._add_value(value, counter)
    chart._history_service._aggregation_to_history(counter)
    if counter % 30 == 0:
        step = -step
    value += step
chart.update_image()
chart.img.save("sequentialchart/test_chart_fullborder.png".format(counter))
chart.stop()
print("-----------------------------------------------------------------------")
chart = SequentialChart(config["display-server"]["renderer"]["elements"][0], update_available, mqtt_client, logger)
chart.start()
chart._border_bottom = False
chart._border_top = False
chart._border_left = False
chart._border_right = False
chart._update_margins()
value = 0
step = -1
for counter in range(0, 300):
    chart._history_service._add_value(value, counter)
    chart._history_service._aggregation_to_history(counter)
    if counter % 30 == 0:
        step = -step
    value += step
chart.update_image()
chart.img.save("sequentialchart/test_chart_noborder.png".format(counter))
chart.stop()
print("-----------------------------------------------------------------------")
try:
    del config["display-server"]["renderer"]["elements"][0]["range-minimum"]
except KeyError:
    pass
try:
    del config["display-server"]["renderer"]["elements"][0]["range-maximum"]
except KeyError:
    pass
chart = SequentialChart(config["display-server"]["renderer"]["elements"][0], update_available, mqtt_client, logger)
chart.start()
value = 0
step = -1
for counter in range(0, 300):
    chart._history_service._add_value(value, counter)
    chart._history_service._aggregation_to_history(counter)
    if counter % 30 == 0:
        step = -step
    value += step
chart.update_image()
chart.img.save("sequentialchart/test_chart_autorange.png".format(counter))
chart.stop()
print("-----------------------------------------------------------------------")
config["display-server"]["renderer"]["elements"][0]["range-minimum"] = 10
try:
    del config["display-server"]["renderer"]["elements"][0]["range-maximum"]
except KeyError:
    pass
chart = SequentialChart(config["display-server"]["renderer"]["elements"][0], update_available, mqtt_client, logger)
chart.start()
value = 0
step = -1
for counter in range(0, 300):
    chart._history_service._add_value(value, counter)
    chart._history_service._aggregation_to_history(counter)
    if counter % 30 == 0:
        step = -step
    value += step
chart.update_image()
chart.img.save("sequentialchart/test_chart_range_min.png".format(counter))
chart.stop()
print("-----------------------------------------------------------------------")
config["display-server"]["renderer"]["elements"][0]["range-maximum"] = 20
try:
    del config["display-server"]["renderer"]["elements"][0]["range-minimum"]
except KeyError:
    pass
chart = SequentialChart(config["display-server"]["renderer"]["elements"][0], update_available, mqtt_client, logger)
chart.start()
value = 0
step = -1
for counter in range(0, 300):
    chart._history_service._add_value(value, counter)
    chart._history_service._aggregation_to_history(counter)
    if counter % 30 == 0:
        step = -step
    value += step
chart.update_image()
chart.img.save("sequentialchart/test_chart_range_max.png".format(counter))
chart.stop()
print("-----------------------------------------------------------------------")
config["display-server"]["renderer"]["elements"][0]["range-maximum"] = 20
config["display-server"]["renderer"]["elements"][0]["range-minimum"] = 10
chart = SequentialChart(config["display-server"]["renderer"]["elements"][0], update_available, mqtt_client, logger)
chart.start()
value = 0
step = -1
for counter in range(0, 300):
    chart._history_service._add_value(value, counter)
    chart._history_service._aggregation_to_history(counter)
    if counter % 30 == 0:
        step = -step
    value += step
chart.update_image()
chart.img.save("sequentialchart/test_chart_range_minmax.png".format(counter))
chart.stop()
