import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from pelops import mymqttclient, myconfigtools
from pelops.logging import mylogger
from nikippe.displayserver import DisplayServer
from PIL import Image
import random
import time
from io import BytesIO


# https://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console
# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', msg=None):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    if msg is None:
        print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    else:
        print('\r%s |%s| %s%% %s %s' % (prefix, bar, percent, suffix, msg), end='\r')
    # Print New Line on Complete
    if iteration == total:
        print()


def send_testdata(mqtt_client, topic, aggregation_to_history):
    random.seed()
    value = 15
    value_min = 9.5
    value_max = 20.5
    step = -0.2
    amount = 220
    max_reached = False
    min_reached = False
    for counter in range(0, amount):
        printProgressBar(counter, amount)
        mqtt_client.publish(topic, value)

        if counter % 2 == 0:
            time.sleep(0.1)
            aggregation_to_history(time.time())

        diff_from_center = abs((value_max-value_min)-(value-value_min))
        norm_diff_from_center = diff_from_center / (value_max-value_min)
        rand_max = 3 + (1-norm_diff_from_center) * 7

        value += step
        if value < value_min:
            value = value_min
            step = -step
            min_reached = True
        elif value > value_max:
            value = value_max
            step = -step
            max_reached = True
        elif min_reached and max_reached and random.random()*rand_max <= 1:
            step = -step
    printProgressBar(amount, amount)


img_counter = 0


def receive_img(value):
    global img_counter
    print("- received image {}".format(img_counter))
    img =  Image.open(BytesIO(value))
    img.save("display-server/test_controller_{}.png".format(img_counter))
    img_counter += 1


try:
    os.mkdir("display-server")
except FileExistsError:
    pass

config = myconfigtools.read_config("../tests_unit/config.yaml")

logger = mylogger.create_logger(config["logger"], __name__)

mqtt_client = mymqttclient.MyMQTTClient(config["mqtt"], logger)
mqtt_client.connect()
mqtt_client.subscribe(config["display-server"]["topics-pub-image"], receive_img)

config["display-server"]["send-on-change"] = False
config["display-server"]["send-interval"] = 0
displayserver = DisplayServer(config)
displayserver.start()
print("--------------------------------------------------------------------------------------------------")
print("fill test data.")
send_testdata(mqtt_client, "/test/humidity", displayserver._renderer._elements[0]._history_service._aggregation_to_history)
print("--------------------------------------------------------------------------------------------------")
print("manual write")
displayserver._update()
print("--------------------------------------------------------------------------------------------------")
print("acitvate on update")
displayserver._set_on_change(True)
displayserver._update_thread.start()
mqtt_client.publish("/test/humidity", 1.23)
time.sleep(1)
print("--------------------------------------------------------------------------------------------------")
print("active update interval")
displayserver._set_send_interval(5)
displayserver._loop_thread.start()
time.sleep(2)
mqtt_client.publish("/test/humidity", 3.21)
time.sleep(5)
print("--------------------------------------------------------------------------------------------------")
print("stopping")
displayserver.stop()
mqtt_client.disconnect()
time.sleep(0.5)
print("--------------------------------------------------------------------------------------------------")
print("done")
