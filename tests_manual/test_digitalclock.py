import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from pelops import myconfigtools
from pelops.logging import mylogger
from nikippe.renderer.digitalclock import DigitalClock
import threading
from datetime import datetime


try:
    os.mkdir("digitalclock")
except FileExistsError:
    pass

config = myconfigtools.read_config("../tests_unit/config.yaml")

logger = mylogger.create_logger(config["logger"], __name__)
update_available = threading.Event()
update_available.clear()
dc = DigitalClock(config["display-server"]["renderer"]["elements"][2], update_available, logger)
dc.start()
dc.update_image()
dc.img.save("digitalclock/test_digitalclock.png")
print("waiting for the next 4 full minute events ({})".format(datetime.now()))
for i in range(4):
    update_available.wait()
    update_available.clear()
    print("received image #{}@{}.".format(i+1, datetime.now()))
    dc.update_image()
    dc.img.save("digitalclock/test_digitalclock_{}.png".format(i))
dc.stop()
print("done")